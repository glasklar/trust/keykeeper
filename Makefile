ROOTDIR = catalyst/keykeeper/root_overlay

all: commit

version:
	printf "keykeeper-%s\n" $$(git describe --tag --always HEAD) > $(ROOTDIR)/etc/issue.keykeeper

sums: version
	find $(ROOTDIR) -type f -print0 | xargs --null sha512sum > SHA512.txt

sums_signed: sums
	gpg --clearsign SHA512.txt
	rm SHA512.txt

commit: sums_signed
	git add SHA512.txt.asc $(ROOTDIR)/etc/issue.keykeeper
	git commit -m "Version and checksums"

clean:
	-rm SHA512.txt SHA512.txt.asc $(ROOTDIR)/etc/issue.keykeeper

.PHONY: version sums sums_signed commit
