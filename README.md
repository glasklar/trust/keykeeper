Code for building a smallish Gentoo system for generating
cryptographic keys.

Boot it on a laptop, generate some keys, export them to a HSM and put
the laptop away in a safe place.

See catalyst/README for instructions on how to build the system.
