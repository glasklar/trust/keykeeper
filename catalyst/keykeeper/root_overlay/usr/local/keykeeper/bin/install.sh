#! /bin/sh

set -o errexit
set -o nounset

# Clone sigsum/core/key-mgmt
sigsum_key_mgmt_install() {
    name="$1"; shift
    mkdir -p /usr/local/src
    git -C /usr/local/src clone --branch "$name" --depth 1 https://git.glasklar.is/sigsum/core/key-mgmt.git
}

sigsum_key_mgmt_install v0.2.3
