0. Prepare two USB sticks

 One bootable with the gentoo installer.
 Another one with an ext4 filesystem with the keykeeper stage4 tar ball.

1. Boot the Chromebook from the bootable USB stick

 TODO: describe Developer Mode and how to enable USB booting

 After kernel booting, this should be printed:

 livecd ~ # 

Set date (format YYYY-MM-DD HH:MM TIMEZONE):

 date -s "2024-07-06 12:12 CEST"

2. Prepare the storage device by partitioning it and creating

 export DEV=/dev/sdX
 parted $DEV mklabel gpt
 parted $DEV unit mib mkpart ESP fat32 1 512
 parted $DEV set 1 boot on
 parted $DEV name 1 efi
 parted $DEV unit mib mkpart primary ext2 512 1024
 parted $DEV name 2 boot
 parted $DEV unit mib mkpart primary ext4 1024 100%
 parted $DEV name 3 rootfs
 mkfs.vfat -F 32 ${DEV}p1
 mkfs.ext2 ${DEV}p2
 mkfs.ext4 ${DEV}p3

3. Unpack the keykeeper tar ball in the newly created filesystems and
   chroot into the root

 mkdir /mnt2; mount /dev/sdY1 /mnt2
 mount ${DEV}p3 /mnt/gentoo
 cd /mnt/gentoo
 mkdir boot; mount ${DEV}p2 boot
 mkdir boot/efi; mount ${DEV}p1 boot/efi
 tar xpf /mnt2/stage4-amd64-*.tar.bz2 --xattrs-include='*.*' --numeric-owner
 chown root:root .

 mount -t proc proc proc
 mount -R /sys sys; mount --make-rslave sys
 mount -R /dev dev; mount --make-rslave dev
 mount -R /run run; mount --make-rslave run
 chroot .

 . /etc/profile; export PS1="(chroot) $PS1"

4. Clear the root password

 passwd -d root

5. Install GRUB

 mkdir /boot/grub
 grub-mkconfig -o /boot/grub/grub.cfg
 grub-install --target=x86_64-efi --efi-directory=/boot/efi --bootloader-id=gentoo --recheck

6. Unmount filesystems and reboot into the new kernel

 exit
 cd ..
 mount | awk '/\/mnt\/gentoo/{print $3}' | sort -r | while read -r mp; do umount $mp; done
 reboot
