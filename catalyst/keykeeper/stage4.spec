version_stamp: keykeeper-24cb.5
subarch: amd64
target: stage4
rel_type: default
profile: default/linux/amd64/23.0/hardened/selinux
source_subpath: stage3-amd64-hardened-selinux-openrc-20240630T170408Z
snapshot_treeish: c5e745425ae2052fec98cb878c5b715eb62350ca
portage_confdir: /home/builder/keykeeper/catalyst/keykeeper/portage
repos: /home/builder/keykeeper/catalyst/keykeeper/repos/local
cflags: -O2 -pipe

stage4/use: acpi bzip2 cli crypt glib ipv6 llvm openssl posix readline ssl threads udev unicode usb zlib
stage4/packages:
  app-crypt/yubihsm-connector
  app-crypt/yubihsm-shell
  app-editors/vim
  dev-vcs/git
  sys-apps/coreutils
  sys-apps/usbutils
  sys-boot/grub
stage4/unmerge:
  dev-lang/go
  sys-apps/help2man
stage4/empty:
  /usr/src
  /usr/include
  /usr/lib/python3.11
  /usr/share/doc
  /usr/share/genkernel
  /usr/share/texinfo
  /usr/share/man
  /usr/share/sgml
  /var/cache
  /var/db/repos/gentoo
stage4/fsscript: /home/builder/keykeeper/catalyst/keykeeper/fsscript.sh
stage4/root_overlay: /home/builder/keykeeper/catalyst/keykeeper/root_overlay
stage4/users: prov

boot/kernel: keykeeper
#boot/kernel/gk_mainargs:
boot/kernel/keykeeper/sources: gentoo-sources
boot/kernel/keykeeper/config: linux-defconfig-6.1.28-chromebook
#boot/kernel/keykeeper/gk_kernargs:
#boot/kernel/keykeeper/use:
